package com.daifaming.restfulservice.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Service
public class JedisClientSingle
{
    @Autowired
    private JedisPool jedisPool;

    public static final String PREFIX ="restful-";

    public String get(String key)
    {
        Jedis jedis = jedisPool.getResource();
        String value = jedis.get(String.format("%s%s",PREFIX,key));
        jedis.close();
        return value;
    }

    public void set(String key, String value)
    {
        Jedis jedis = jedisPool.getResource();
        jedis.set(String.format("%s%s",PREFIX,key), value);
        jedis.close();
    }
}
