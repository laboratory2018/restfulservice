package com.daifaming.restfulservice.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author Administrator
 */
public class SpringContextUtil implements ApplicationContextAware
{
    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.context.ApplicationContextAware#setApplicationContext
     * (org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        synchronized (applicationContext)
        {
            setApplicationContextValue(applicationContext);
        }

    }

    private static ApplicationContext applicationContext;

    public SpringContextUtil()
    {
    }

    public static ApplicationContext getApplicationContext()
    {
        return applicationContext;
    }

    public static Object getBean(String name)
    {
        return applicationContext.getBean(name);
    }

    public static void setApplicationContextValue(ApplicationContext applicationContextNew)
    {
        applicationContext = applicationContextNew;
    }

}
