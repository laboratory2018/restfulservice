package com.daifaming.restfulservice.delegate;

public interface HealthCheckDelegate
{
    void check();
}
