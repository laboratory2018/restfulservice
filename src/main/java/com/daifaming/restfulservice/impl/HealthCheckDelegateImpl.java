package com.daifaming.restfulservice.impl;

import com.daifaming.restfulservice.common.utils.JedisClientSingle;
import com.daifaming.restfulservice.delegate.HealthCheckDelegate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HealthCheckDelegateImpl implements HealthCheckDelegate
{

    @Autowired
    private JedisClientSingle jedisClientSingle;

    @Override
    public void check()
    {
        jedisClientSingle.set("key","value");
        String str = jedisClientSingle.get("key");
        log.info("key is {}", str);

        log.info("health check success.");
    }
}
