package com.daifaming.restfulservice.resource;

import com.daifaming.restfulservice.delegate.HealthCheckDelegate;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/healthCheck")
public class HealthCheck
{
    @Autowired
    private HealthCheckDelegate healthCheckDelegate;

    @GET
    public void healthCheck()
    {
        healthCheckDelegate.check();
    }
}
