package com.daifaming.restfulservice.scheduler;

public abstract class SchedulerTask
{

    abstract void execute();

}
