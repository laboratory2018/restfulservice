package com.daifaming.restfulservice.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TestSchedulerTask extends SchedulerTask
{

    @Value("${quartz.testJob.flag}")
    private boolean flag;

    public void execute()
    {
        if (flag)
        {
            log.info("cron job success!");
        }
    }

}
