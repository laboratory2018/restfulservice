/**
 *
 */
package com.daifaming.restfulservice.tool;

import com.daifaming.restfulservice.common.constants.CharsetNameConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Administrator
 */
@Slf4j
public class EncryptUtil
{
    /**
     * 利用MD5进行加密
     *
     * @param str 待加密的字符串
     * @return 加密后的字符串
     */
    private static String encoderByMd5(String str)
    {
        // 确定计算方法
        MessageDigest md5;
        String newStr = null;
        try
        {
            md5 = MessageDigest.getInstance("MD5");
            newStr = new String(Base64.encodeBase64(md5.digest(str.getBytes(CharsetNameConstant.UTF_8))), CharsetNameConstant.UTF_8);
        }
        catch (NoSuchAlgorithmException e)
        {
            log.error("error msg is:{}", e.getMessage());
        }
        catch (UnsupportedEncodingException e)
        {
            log.error("error msg is:{}", e.getMessage());
        }
        // 加密后的字符串
        return newStr;
    }

    /**
     * 判断用户密码是否正确
     *
     * @param newpasswd 用户输入的密码
     * @param oldpasswd 数据库中存储的密码－－用户密码的摘要
     * @return boolean 用户密码是否正确
     */
    public static boolean checkpassword(String newpasswd, String oldpasswd)
    {
        return encoderByMd5(newpasswd).equals(oldpasswd);
    }
}
