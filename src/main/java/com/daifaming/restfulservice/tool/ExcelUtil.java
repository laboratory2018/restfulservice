package com.daifaming.restfulservice.tool;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class ExcelUtil
{

    /**
     * @param map
     * @param path
     * @MethodName : generateExcel
     * @Description : 生成excel
     */
    public void generateExcel(Map<String, List<Object>> map, String path, String fileName)
    {

        //创建excel
        HSSFWorkbook wb = createExcel(map);
        //写入excel
        write(wb, path, fileName);
    }

    /**
     * @param map
     * @return
     * @MethodName : createExcel
     * @Description : 创建excel
     */
    private HSSFWorkbook createExcel(Map<String, List<Object>> map)
    {
        //创建excel
        //创建HSSFWorkbook对象
        HSSFWorkbook wb = new HSSFWorkbook();

        //遍历创建每一个sheet
        map.forEach((k, v) -> {
            HSSFSheet sheet = wb.createSheet(k);
            getRows(v, sheet);
        });

        return wb;

    }

    /**
     * @param datas
     * @param sheet
     * @MethodName : getRows
     * @Description : 遍历创建每一行
     */
    private void getRows(List<Object> datas, HSSFSheet sheet)
    {
        datas.forEach(data -> {
            HSSFRow row = sheet.createRow(datas.indexOf(data));
            //遍历创建每一行
            getCells(data, row);
        });
    }

    /**
     * @param data
     * @param row
     * @MethodName : getCells
     * @Description : 遍历创建每一个cell
     */
    private void getCells(Object data, HSSFRow row)
    {

        try
        {
            Class<? extends Object> cls = data.getClass();
            // 获取类中的所有定义字段
            Field[] fields = cls.getDeclaredFields();
            List<Field> lists = Arrays.asList(fields);

            for (Field field : lists)
            {
                // 如果不为空，设置可见性，然后返回
                field.setAccessible(true);

                HSSFCell cell = row.createCell(lists.indexOf(field));
                String value = "" + field.get(data);
                cell.setCellValue(value);
            }
        }
        catch (Exception e)
        {
            log.error("faild to get Cell!", e);
        }

    }

    /**
     * @param wb
     * @param path
     * @MethodName : getString
     * @Description : 将创建的excel写入文件中
     */
    private void write(HSSFWorkbook wb, String path, String fileName)
    {

        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String time = sdf.format(now);
        log.info("current time is {}", time);

        String targetPath = path + File.separator + fileName + time + ".xls";
        log.info("file path is {}", targetPath);

        FileOutputStream output = null;
        try
        {

            mkDir(new File(path));
            File file = new File(targetPath);

            if (!file.exists())
            {
                boolean is = file.createNewFile();
                if (is)
                {
                    output = new FileOutputStream(targetPath);
                    wb.write(output);
                    output.flush();
                }
            }

        }
        catch (IOException e)
        {
            log.error("write excel into file error!", e);
        }
        finally
        {
            try
            {
                if (null != output)
                {
                    output.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * 递归生成目录
     *
     * @param file
     */
    public void mkDir(File file) throws IOException
    {
        boolean isCreate;
        if (!file.getParentFile().exists())
        {
            mkDir(file.getParentFile());
        }

        isCreate = file.mkdir();

        if (!isCreate)
        {
            log.error("faild to create file");
            throw new IOException();
        }

    }

    public static ExcelUtil getInstance()
    {
        return new ExcelUtil();
    }
}